module com.ace {

    requires javafx.controls;
    requires javafx.graphics;

    opens com.ace to javafx.graphics;
}